# Check the PYTHONPATH environment variable before beginning to ensure that the
# top-level directory is included.  If not, append the top-level.  This allows
# the modules within the .../project/ directory to be discovered.
import sys
import os


print('Creating database tables for RwandaFitness_app...')



if os.path.abspath(os.curdir) not in sys.path:
    print('...missing directory in PYTHONPATH... added!')
    sys.path.append(os.path.abspath(os.curdir))





from project import db
from project.models import Gym, User


# drop all of the existing database tables
db.drop_all()
 
 
# create the database and the database table
db.create_all()

# insert user data
# Insert user data
user1 = User(email='patkennedy79@yahoo.com', plaintext_password='password1234')
user2 = User(email='kennedyfamilyrecipes@gmail.com', plaintext_password='PaSsWoRd')
user3 = User(email='blaa@blaa.com', plaintext_password='MyFavPassword')
db.session.add(user1)
db.session.add(user2)
db.session.add(user3)
admin_user = User(email='<email>', plaintext_password='<password>', role='admin')
db.session.add(admin_user)

# Commit the changes for the users
db.session.commit()
 
# insert gym data
gym1 = Gym('Cali Fitness', ' The best Gym in Rwanda located in Nyarutarama!' , admin_user.id, False)
gym2 = Gym('Be Fit', 'The popular gym near Sonatubes', admin_user.id, True)
gym3 = Gym('Waka Fitness', 'The most VIP gym in Kigali.', user1.id, True)
db.session.add(gym1)
db.session.add(gym2)
db.session.add(gym3)

 
# commit the changes
db.session.commit()



print('Done!!...')