#################
#### imports ####
#################
 
from flask import render_template, request, Blueprint, redirect, url_for, flash
from flask_login import current_user, login_required
from project import db, app
from project.models import Gym, User
from .forms import AddGymForm
 
 
################
#### config ####
################
 
gyms_blueprint = Blueprint('gyms', __name__)


def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ), 'info')
 
 
################
#### routes ####
################
 


@gyms_blueprint.route('/')
def public_gyms():
    all_public_gyms = Gym.query.filter_by(is_public=True)
    return render_template('public_gyms.html', public_gyms=all_public_gyms)  


@gyms_blueprint.route('/gyms')
@login_required
def user_gyms():
    all_user_gyms = Gym.query.filter_by(user_id=current_user.id)
    return render_template('user_gyms.html', user_gyms=all_user_gyms)


@gyms_blueprint.route('/add', methods=['GET', 'POST'])
def add_gym():
    form = AddGymForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            new_gym = Gym(form.gym_title.data, form.gym_description.data, current_user.id, True)
            db.session.add(new_gym)
            db.session.commit()
            flash('New Gym, {}, added!'.format(new_gym.gym_title), 'success')
            return redirect(url_for('gyms.public_gyms'))
        else:
            flash_errors(form)   
            flash('ERROR! Gym was not added.', 'error')
    return render_template('add_gym.html', form=form)


@gyms_blueprint.route('/gym/<gym_id>')
def gym_details(gym_id):
    gym_with_user = db.session.query(Gym, User).join(User).filter(Gym.id == gym_id).first()
    if gym_with_user is not None:
        if gym_with_user.Gym.is_public:
            return render_template('gym_detail.html', gym=gym_with_user)
        else:
            if current_user.is_authenticated and gym_with_user.Gym.user_id == current_user.id:
                return render_template('gym_detail.html', gym=gym_with_user)
            else:
                flash('Error! Incorrect permissions to access this gym.', 'error')
    else:
        flash('Error! Gym does not exist.', 'error')
    return redirect(url_for('gyms.public_gyms'))
