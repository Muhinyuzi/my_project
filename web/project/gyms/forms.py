# project/recipes/forms.py
 
 
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired
 
 
class AddGymForm(Form):
    gym_title = StringField('Gym Title', validators=[DataRequired()])
    gym_description = StringField('Gym Description', validators=[DataRequired()])