

# project/test_gyms.py
 
 
import os
import unittest
 
from project import app, db
 
 
TEST_DB = 'test.db'
 
 
class ProjectTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + \
            os.path.join(app.config['BASEDIR'], TEST_DB)
        self.app = app.test_client()
        db.create_all()
 
        self.assertEquals(app.debug, False)
 
    # executed after each test
    def tearDown(self):
        db.session.remove()
        db.drop_all()
 
 
    ###############
    #### tests ####
    ###############
 
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertIn(b'Gyms in Rwanda!', response.data)
        self.assertIn(b'Cali Fitness', response.data)
        self.assertIn(b'Be Fit', response.data)
        self.assertIn(b'Waka Fitness', response.data)
        self.assertIn(b'Add Gym', response.data)
 
    def test_main_page_query_results(self):
        response = self.app.get('/add', follow_redirects=True)
        self.assertIn(b'Add a New Gym', response.data)
 
    def test_add_gym(self):
        response = self.app.post(
            '/add',
            data=dict(gym_title='xxxxx',
                      gym_description='xxxxx'),
            follow_redirects=True)
        self.assertIn(b'New Gym, xxxxx, added!', response.data)
 
    def test_add_invalid_gym(self):
        response = self.app.post(
            '/add',
            data=dict(gym_title='',
                      gym_description='xxxxx'),
            follow_redirects=True)
        self.assertIn(b'ERROR! Gym was not added.', response.data)
        self.assertIn(b'This field is required.', response.data)
 
 
if __name__ == "__main__":
    unittest.main()